/* PNG Tagger -- a program for editing PNG image header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief Global function and data type / structure definitions. All global
 *         functions are in the Pt namespace.
 */

#include "global.h"

#include <QString>
#include <QRegExp>

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#ifdef Q_WS_WIN
#include <windows.h>
HANDLE hCon;
#endif


using namespace std;


void Pt::attention(const char* text, const string& msg)
{
#ifdef Q_WS_WIN
    hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hCon, 4);  // 4 means dark red text.
    cerr << text;
    SetConsoleTextAttribute(hCon, 15); // 15 means white text.
#else
    cerr << "\n\033[1;31m" << text << ": \033[0m";
#endif

    cerr << msg;
    cerr << "\n";
}


void Pt::print_warning(const string& msg)
{
    Pt::attention("Warning", msg);
}


void Pt::print_warning(const char* _msg)
{
    string msg(_msg);
    Pt::print_warning(msg);
}


void Pt::print_error(const string& msg, int status)
{
    Pt::attention("Error", msg);
    exit(status);
}


void Pt::print_error(const char* _msg, int status)
{
    string msg(_msg);
    Pt::print_error(msg, status);
}


bool Pt::is_PNG(const char* file)
{
    ifstream ifs ( file , ifstream::binary );

    ifs.get();
    const char p = char(ifs.get());
    const char n = char(ifs.get());
    const char g = char(ifs.get());
    return ( p == 'P' && n == 'N' && g == 'G' );
}


bool Pt::is_numeric(const char* str)
{
    // This is a somewhat weak test, but probably good enough.
    // A better test would check every char in the array with isdigit().
    return ( ( (str[0] == '-') && isdigit(str[1]) ) || isdigit(str[0]) );
}


bool Pt::is_valid_tag(const QString& tag)
{
    // a stringent format test needs to be applied here
    return tag.contains(QRegExp("\\w"));
}
