/* PNG Tagger -- a program for editing PNG image header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief cli_actions member function definitions.
 */

#include "CLI/cli_actions.h"

#include <QImage>
#include <QString>

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <sstream>
#include <cctype>


using namespace std;


cli_actions::cli_actions(int _argc, char** _argv)
{
    argc = _argc;
    argv = _argv;


    make_flag_map();
    make_param_map();

    // set various quantities to their default values

    image_file = NULL;
    tag_file = NULL;

    the_tag = 2;
}


void cli_actions::display_meta_data()
{
    cerr << "hello\n";
}


void cli_actions::edit_tag()
{
    QImage image(image_file);

    image.setText("Description", QString(argv[the_tag]));

    image.save(image_file);
}


void cli_actions::make_flag_map()
{
    flag["--tag"     ] = __tag;
    flag["--info"    ] = __info;
}


void cli_actions::make_param_map()
{
    param["Parameter"     ] = Parameter;
}


void cli_actions::parse_cl()
{
    uint arg_number = 1;

    while ( arg_number < uint(argc) )
    {
        if ( is_char_flag(argv[arg_number]) )
        {
            arg_number += interpret_char_flags(arg_number);
        }
        else if ( is_long_flag(argv[arg_number]) )
        {
            arg_number += interpret_long_flags(arg_number);
        }
        else if (argc == 2) // No flags were found; assume argv[1] is image_file
        {
            image_file = argv[1];
            my_task = display;
            arg_number++;
        }
        else if ( arg_number == (uint(argc) - 1) )
        {
            // assume the final argument is the file name, input_file0.

            char* arg = argv[arg_number];
            arg_error(is_char_flag(arg), arg);

            tag_file = arg;
            arg_number++;
        }
        else
        {
            string msg = "There is probably a syntax error. I don't understand";
            msg += " what you mean by \"" + string(argv[arg_number]) + "\". ";
            msg += "I'll try to ignore it, but I thought you should know about";
            msg += " this potential problem.\n";
            Pt::print_warning(msg);
            arg_number++;
        }
    }

    run();
}


uint cli_actions::interpret_long_flags(const uint arg_n)
{
    string opt_string(argv[arg_n]);

    char* arg;

    string msg;

    switch ( flag[opt_string] )
    {
    case flag_error:

        msg += "I don't recognize the flag \"" + opt_string + "\"\n";
        Pt::print_error(msg, 1);

    case __tag:

        arg = check_bounds(arg_n + 2);
        my_task = edit;
        the_tag = arg_n + 1;
        image_file = arg;
        return 3;
        break;

    case __info:

        arg = check_bounds(arg_n + 1);

        my_task = display;

        return 2;
        break;

    default:

        return 1;
    }
}


uint cli_actions::interpret_char_flags(const uint arg_number)
{
    uint string_idx = 1;
    char opt, *arg;
    string msg;

    do
    {
        opt = argv[arg_number][string_idx];

        switch (opt)
        {
        case 't':

            my_task = edit;
            string_idx++;
            break;

        case 'd':

            arg = check_bounds(arg_number + 1);
            my_task = display;
            cerr << arg << endl;
            // this does not do anythyng yet

        default:

            if (opt == '\0')
            {
                return 1;
            }
            else
            {
                msg += "I don't recognize the flag \"" + string(&opt) + "\"\n";
                Pt::print_error(msg, 1);
            }
        }
    }
    while (opt != '\0');

    return 1;
}


bool cli_actions::is_char_flag(const char* str) const
{
    return ( (str[0] == '-') && isalpha(str[1]) );
}


bool cli_actions::is_long_flag(const char* str) const
{
    return ( (str[0] == '-') && (str[1] == '-') && isalpha(str[2]) );
}


void cli_actions::arg_error(const bool b, const char* str) const
{
    if (b)
    {
        string msg = "\"" + string(str) + "\" is an invalid argument.\n";
        Pt::print_error(msg, 1);
    }
}


char* cli_actions::check_bounds(const uint n) const
{
    if (n < (uint)argc)
    {
        return argv[n];
    }
    else
    {
        string msg = "There appears to be a missing argument.\n";
        Pt::print_error(msg, 1);
        exit(1); // only to avoid compiler error.
    }
}


void cli_actions::parse_tag_file()
{
    if ( Pt::is_PNG(tag_file) )  // The parameter file is a PNG file
    {
        parse_PNG_tag_file();
    }
    else // The tag_file file is a text file.
    {
        parse_text_tag_file();
    }
}


void cli_actions::parse_PNG_tag_file()
{
    QImage qimage(tag_file);

    QStringList lines = qimage.text().split("\n", QString::SkipEmptyParts);

    for (int i = 0; i < lines.length(); i++)
    {
        if ( !lines[i].contains("Version") && lines[i].at(0).isUpper() )
        {
            parse_tag_file_line(lines[i].toLocal8Bit(), i + 1);
        }
    }
}


void cli_actions::parse_text_tag_file()
{
    ifstream parameters ( tag_file , ifstream::in );

    if (!parameters)
    {
        string msg = "unable to open " + string(tag_file) + "\n";
        Pt::print_error(msg, 1);
    }

    while (parameters.good())
    {
        char line[256];

        static uint line_number = 0;

        ++line_number;

        parameters.getline(line, 256);

        parse_tag_file_line(line, line_number);
    }

    parameters.close();
}


void cli_actions::parse_tag_file_line(const char* line, const uint line_number)
{
    QString str(line);

    int comnt_idx = str.indexOf('#'); // index of the comment character.

    // remove comments from the string

    if (str.contains("#")) {str.chop( str.size() - comnt_idx );}

    // The string will be broken into space-delimited words, so replace all
    // possible delimiters with spaces.

    str.replace(QString("\t"), QString(" "));
    str.replace(QString("="), QString(" "));
    str.replace(QString(":"), QString(" "));

    if ( str.contains(";") )
    {
        // recursively split lines into semicolon delimited sub-lines.

        QStringList sub_str = str.split(";", QString::SkipEmptyParts);

        for (int s = 0; s < sub_str.size(); s++)
        { parse_tag_file_line(sub_str[s].toStdString().c_str(), line_number); }
    }
    else
    {
        QStringList list = str.split(" ", QString::SkipEmptyParts);

        if (list.size() == 2)
        {
            interpret_tag_file_line(list);
        }
        else if ( (list.size() == 1) || (list.size() > 2) )
        {
            string msg = "I don't understand the parameter file line # ";
            msg += QString::number(line_number).toStdString() + ":\n\n\"";
            msg += list.join(" ").toStdString() + "\"\n";
            Pt::print_error(msg, 1);
        }
    }
}


void cli_actions::interpret_tag_file_line(const QStringList& line)
{
    QString key = line[0], value = line[1];

    string msg;

    bool ok;

    switch ( param[key.toStdString()] )
    {
    case Parameter:      cerr << "This works, but desn't do anthing yet"; break;

    case Parameter2:     cerr << "example code" << value.toUInt(&ok);      break;

    case param_error:

        msg = "I don't recognize \"" + key.toStdString() + "\"\n";
        Pt::print_error(msg, 1);
    }

    if (!ok)
    {
        msg = "the tag file value \"" + value.toStdString();
        msg += "\" was not successfully converted to a numerical type.\n";
        Pt::print_error(msg, 1);
    }
}
