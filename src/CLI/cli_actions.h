/* PNG Tagger -- a program for editing PNG image header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
  \brief Performs actions specified on the command line; implements the command
         line interface.

  \todo Write documentation / instructions for adding a new command line option.

 ******************************************************************************/

#ifndef CLI_ACTIONS_H
#define CLI_ACTIONS_H

#include "global.h"

#include <QString>
#include <QStringList>

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>


/*! \brief Reads the command line flags and parses the parameter file.

    This class contains the command line interface. An object of this class can
    parse the command line flags, read the parameter file, and perform actions
    based on the flags and parameters. */
class cli_actions
{
protected:

    int argc;
    char** argv;

    // file names
    char* image_file;
    char* tag_file;

    int the_tag;

    //  general types of tasks performed.
    enum tasks {edit, display } my_task;

    /*  This enumeration, along with a std::map<string, string_flags>, object
        allows us to use strings as values in a switch statement. Refer to
        interpret_long_flags() to see exactly how this is done. */
    enum long_flags
    {
        flag_error,
        __tag,
        __info
    };

    //   An associative array for connecting string flags with elements of the
    //  long_flag enumeration.
    std::map< std::string, long_flags > flag;

    enum parameters
    {
        param_error,
        Parameter,
        Parameter2
    };

    std::map< std::string, parameters > param;

public:

    cli_actions(int _argc, char** _argv);

    /*! \brief Parses the command line arguments.

     Reads the arguments in argv. Interprets any argument beginning with '-'
     or '--' as a special keyword.

     In general, there are two types of keyword functions. Some specify an
     input value and others specify an action.  For instance, in the
     current implementation, the -g, -n, --view, -i, --iview flags
     only indicate that an action should be performed, while the -t, -a,
     --beg, --end, --out, --theta, --phi, -c, and --frames flags specify that
     the next argument on the command line is to be interpreted as an input
     value of some kind.

     Note that we strictly follow a naming convention whereby flags beginning
     with '-' are single character, "short" flags, while flags beginning with
     '--' are "long" flags, consisting of a string.

     Short flags can be combined. For instance, rather than using
     <PRE>

     ./gsnap -n -t 001110 filename,

     </PRE>
     it is sufficient to use
     <PRE>

     ./gsnap -nt 001110 filename.

     </PRE>
     However, input flags cannot be followed by another flag. For instance,
     <PRE>

     ./gsnap -tn 001110 filename.

     </PRE>
     Will not yield the expected result because the "-n" flag will be ignored.
     */
    void parse_cl();

    /*! \brief Parses a tag file. */

    void parse_tag_file();

protected:

    /*! Performs the task specified on the command line.*/
    void run()
    {
        switch (my_task)
        {
        case edit:    edit_tag();             break;

        case display: display_meta_data();    break;
        }
    }

    /*! if the "--view" flag was specified, this function creates an image of
        the snapshot along the z-axis and saves the image to disk as a PNG file.

        If "--iview", this function launches the interactive snapshot
        viewer, which allows the user to rotate and zoom the snapshot file and
        change the gamma value.

        If "-g", this function renders an image of the gas component
        of a snapshot using a volume rendering scheme.

        If "--light", this function renders an image of the stellar component
        using a volume rendering scheme. */
    void display_meta_data();

    void edit_tag();

    /*! Tests whether the argument string is a command line (short) flag.
        Returns true if the flag is short and false otherwise. */
    bool is_char_flag(const char* str) const;

    /*! Tests whether the argument string is a command line (long) flag.
        Returns true if the flag is long and false otherwise. */
    bool is_long_flag(const char* str) const;

    /*! Reports errors. If b is true, str is used as part of an error message
        and execution is terminated. */
    void arg_error(const bool b, const char* str) const;

    /*! Performs bounds-checking on the argv array in order to prevent
        segmentation faults. */
    char* check_bounds(const uint n) const;

    /*! Parses a text parameter file. */
    void parse_text_tag_file();

    /*! Parses parameters stored in the meta data of a PNG image file. */
    void parse_PNG_tag_file();

    /*! Parses a parameter string (one line the parameter file) */
    void parse_tag_file_line(const char* line, const uint line_number = 0);

    /*! Interprets --long_flags. */
    uint interpret_long_flags(const uint arg_n);

    /*! Interprets short, single character flags (like -s). */
    uint interpret_char_flags(const uint arg_number);

    /*! Interprets parameter file keyword-value pairs. */
    void interpret_tag_file_line(const QStringList& line);

    /*! Sets up an associative array mapping long command flags (strings) to
        the long_flags enumeration entries. */
    void make_flag_map();

    /*! Sets up an associative array mapping parameter file entries (strings)
        to the parameters enumeration entries. */
    void make_param_map();
};

#endif // CLI_ACTIONS_H
