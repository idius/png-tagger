/* PNG Tagger -- a program for editing PNG image header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
 *  \brief PNG Tagger's main() function.
 */

#include "GUI/gui_main.h"
#include "CLI/cli_actions.h"


using namespace std;

int main(int argc, char** argv)
{
    if (argc > 1) // in this case, we launch the command line interface.
    {
        cli_actions cli(argc, argv);

        cli.parse_cl();

        return 0;
    }
    else if (argc == 1) // in this case, we launch the GUI.
    {
        QApplication app(argc, argv);

        gui_main_window gui(&app);

        gui.show();

        return app.exec();
    }

    return 1;
}
