/* compile with

         'g++ spell_test.cc -lenchant -I/usr/include/enchant/ -o spell_test'
   This assumes that the Enchant library headers are installed in the directory

          /usr/include/enchant/

   and the Enchant library and one of its back-ends has been built and installed
   in a location that the linker is aware.
   (http://www.abisource.com/projects/enchant/)
*/

#include <memory>
#include <iostream>
#include <vector>
#include <string>

#include <enchant++.h>

using namespace std;

int main ()
{
    enchant::Broker* broker = enchant::Broker::instance ();
    std::auto_ptr<enchant::Dict> dict (broker->request_dict ("en_US"));

    const char* check_checks[] = { "Nathaniel", "helllo" };

    for (int i = 0; i < (sizeof (check_checks) / sizeof (check_checks[0])); ++i)
    {
        cerr << "word = " << check_checks[i] << " ";

        if (dict->check (check_checks[i]))
        {
            cerr << " is spelled corretly " << endl;
        }
        else
        {
            cerr << " is NOT spelled corretly. I suggest: " << endl;

            vector<string> suggestions;

            dict->suggest(string(check_checks[i]), suggestions);

            for (int j = 0; j < suggestions.size(); ++j)
            {
                cerr << suggestions[j] << endl;
            }
        }
    }

    return 0;
}
