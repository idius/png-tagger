/* PNG Tagger -- a program for editing PNG img header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "GUI/gui_main.h"

#include <iostream>

using namespace std;


gui_main_window::gui_main_window(QApplication* app, QMainWindow* parent)
    : QMainWindow(parent)
{
    application = app;
    current_directory = "";
    current_file_name = "";
    current_img_index = 0;
    set_connections();
    set_modified(false);
    scene.set_parent(graphicsView);
    current_scale = 1.0;
}


gui_main_window::~gui_main_window()
{
    cerr << "\nGoodbye!\n";
}


int gui_main_window::open_first_img()
{
    if ( modified ) { warn_unsaved(); }

    current_file_name = QFileDialog::getOpenFileName(this, tr("Open images"),
                        "",
                        "PNG files(*.png *.PNG)");

    if (current_file_name == "")
    {
        return 0;
    }


    current_directory = QFileInfo(current_file_name).dir().absolutePath();
    QDir dir(current_directory, "*.png");
    QStringList img_names = dir.entryList();

    file_list.clear();

    for (int i = 0; i < img_names.length(); i++)
    {
        file_list.append(current_directory + "/" + img_names[i]);
    }

    first_img_index = current_img_index = file_list.indexOf(current_file_name);

    open_img(current_file_name);

    return 0;
}


void gui_main_window::open_next_img()
{
    if ( modified ) { warn_unsaved(); }

    current_img_index++;

    if ( current_img_index == file_list.size() )
    {
        current_img_index = 0;
    }

    if ( file_list.length() > 0 )
    {
        open_img(file_list[current_img_index]);
    }
}


void gui_main_window::open_previous_img()
{
    if ( modified ) { warn_unsaved(); }

    current_img_index--;

    if ( current_img_index == -1 )
    {
        current_img_index = file_list.size() - 1;
    }

    if ( file_list.length() > 0)
    {
        open_img(file_list[current_img_index]);
    }
}


void gui_main_window::open_img(const QString& img_name)
{
    current_file_name = img_name;
    statusbar->showMessage("Loading image...");
    current_img.load(img_name);

    current_img_width = current_img.width();
    current_img_height = current_img.height();

    // Clear previous scene contents

    scene.show_callouts(false);
    scene.free_callouts();
    scene.clear();

    // Display metadata in GUI

    date_line->setReadOnly(false);
    location_line->setReadOnly(false);
    people_box->setReadOnly(false);
    description_box->setReadOnly(false);

    date_line->setText(current_img.text("Date"));
    people_box->setPlainText(current_img.text("People"));
    location_line->setText(current_img.text("Location"));
    description_box->setPlainText(current_img.text("Description"));
    parse_tag_field(current_img.text("Tags"));

    // Add the image to the scene as a Pixmap and set properties so that the
    // image displays nicely and fits into the preview window.

    QGraphicsPixmapItem* pxitem =
        scene.add_pixmap( QPixmap::fromImage(current_img) );
    pxitem->setTransformationMode(Qt::SmoothTransformation);
    graphicsView->setScene(&scene);
    scene.setBackgroundBrush(QBrush(Qt::black));
    scene.setSceneRect(current_img.rect());

    resizeEvent(NULL);

    // Display image and file information in the file info tab

    file_info();

    // Display the image's position in the current directory and it's full path
    // in the status bar.

    QString  status_message = "Image " + QString::number(current_img_index + 1);
    status_message += " of " + QString::number(file_list.size());
    status_message += " : " + img_name;

    statusbar->showMessage(status_message);

    current_scale = 1.0;

    set_modified(false);
}


void gui_main_window::close_img()
{
    current_directory.clear();
    current_file_name.clear();
    current_img_index = 0;
    date_line->clear();
    people_box->clear();
    location_line->clear();
    description_box->clear();
    scene.clear_all();
    scene.setBackgroundBrush(QBrush(Qt::white));
    file_list.clear();
    graphicsView->viewport()->update();
    set_modified(false);
    statusbar->showMessage("");
    scene.free_callouts();
    file_info_browser->clear();
    tag_browser->clear();
    date_line->setReadOnly(true);
    location_line->setReadOnly(true);
    people_box->setReadOnly(true);
    description_box->setReadOnly(true);
}


void gui_main_window::set_date_field(const QString& str)
{
    date_field = str;
    set_modified(true);
}


void gui_main_window::set_people_field()
{
    people_field = people_box->toPlainText();
    set_modified(true);
}


void gui_main_window::append_people_box(const QString& str)
{
    if ( people_box->toPlainText().size() > 1 )
    {
        people_box->setPlainText(people_box->toPlainText() + ", " + str);
    }
    else
    {
        people_box->setPlainText(str);
    }
}


bool gui_main_window::parse_tag_field(QString tag)
{
    tag_field = tag;

    QString tag_html = "<p align=\"center\"><a href=\"*\"><span style=\""
                       "font-weight:600;\">Show All Labels</span></a></p>";

    if ( tag.contains("^|^") )
    {
        QStringList tag_list = tag.split("^|^");

        for (int i = 0; i < tag_list.length(); i++)
        {
            if ( !tag_list[i].contains("@") ) { continue; }

            QStringList link = tag_list[i].split("@", QString::SkipEmptyParts);
            tag_html += "<p><a href=\"" + link[1] + "\">" + link[0] + "</a></p>\n";
            scene.add_callout_string(tag_list[i]);
        }

        tag_browser->setHtml(tag_html);
    }
    else if ( tag.isEmpty() )
    {
        tag_browser->setHtml("This image contains no tags.");
    }

    return true;
}


void gui_main_window::set_location_field(const QString& str)
{
    location_field = str;
    set_modified(true);
}


void gui_main_window::set_description_field()
{
    description_field = description_box->toPlainText();
    set_modified(true);
}


void gui_main_window::append_tag_browser(QString str)
{
    QString s = tag_field + str;
    parse_tag_field(s);
    set_modified(true);
}


void gui_main_window::set_tag_browser(QString str)
{
    tag_browser->clear();
    parse_tag_field(str);
    set_modified(true);
}


bool gui_main_window::save_img()
{
    if ( !modified ) {return false;}

    statusbar->showMessage("Saving image...");

    current_img.setText("Description", description_field);
    current_img.setText("People", people_field);
    current_img.setText("Date", date_field);
    current_img.setText("Location", location_field);
    current_img.setText("Tags", tag_field);
    current_img.save(current_file_name);

    statusbar->showMessage(current_file_name);

    set_modified(false);
    return true;
}


int gui_main_window::save_as()
{
    output_file_name = QFileDialog::getSaveFileName(0, tr("Save image..."));

    if (output_file_name == "")
    {
        return 0;
    }

    return 0;
}


void gui_main_window::resizeEvent ( QResizeEvent* event )
{
    const bool resize_width = (graphicsView->width() < current_img_width);
    const bool resize_height = (graphicsView->height() < current_img_height);
    QRectF rect;

    if ( resize_width || resize_height )
    {
        rect = scene.sceneRect();
    }
    else
    {
        rect = QRectF(0, 0, qreal(graphicsView->width()),
                      qreal(graphicsView->height())
                     );
    }

    graphicsView->fitInView(rect, Qt::KeepAspectRatio);

    if ( event ) { event->accept(); }
}


void gui_main_window::set_forward_cursor()
{
    graphicsView->setCursor(QCursor(QPixmap(":/image/right.png"), 56, 25));
}


void gui_main_window::set_back_cursor()
{
    graphicsView->setCursor(QCursor(QPixmap(":/image/left.png"), 9, 25));
}


void gui_main_window::set_normal_cursor()
{
    graphicsView->setCursor(Qt::ArrowCursor);
}


void gui_main_window::zoom_in()
{
    if ( current_scale < 20 )
    {
        current_scale *= 1.111111;
        graphicsView->scale(1.1111, 1.1111);
    }
}


void gui_main_window::zoom_out()
{
    if ( current_scale > 0.05 )
    {
        current_scale *= 0.9;
        graphicsView->scale(0.9, 0.9);
    }
}


void gui_main_window::set_modified(bool mod)
{
    modified = mod;

    if ( mod )
    {
        saveButton->setEnabled(true);
        saveButton2->setEnabled(true);
    }
    else
    {
        saveButton->setDisabled(true);
        saveButton2->setDisabled(true);
    }
}


void gui_main_window::warn_unsaved()
{
    QMessageBox msgBox;
    msgBox.setText("The image has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Save)
    {
        save_img();
    }
}


void gui_main_window::file_info()
{
    /** \todo clean this up and add more information.*/
    QFileInfo file_info(current_file_name);

    QString html = "<p><span style=\"font-weight:600;\">Image size:</span></p>"
                   "<pre>   ";
    html += QString::number(current_img_width);
    html += " x ";
    html += QString::number(current_img_height);
    html += "</pre><p><span style=\" font-weight:600;\">File size:</span></p>"
            "<pre>   ";

    if ( file_info.size() < 1024 )
    {
        html += QString::number(file_info.size()) + " bytes";
    }
    else if ( file_info.size() < 1048576 )
    {
        html += QString::number(file_info.size() / 1024.0) + " kilobytes";
    }
    else
    {
        html += QString::number(file_info.size() / 1048576.0) + " megabytes";
    }

    html += " </pre><p><span style=\" font-weight:600;\">Bits per pixel:</span>"
            "</p><pre>   ";
    html += QString::number(current_img.depth());
    html += "</pre> <p><span style=\" font-weight:600;\">Created / Status "
            "changed:</span></p><pre>   ";
    html += file_info.created().toString(Qt::SystemLocaleShortDate);
    html += "</pre><p><span style=\" font-weight:600;\">Modified:</span></p>"
            "<pre>   ";
    html += file_info.lastModified().toString(Qt::SystemLocaleShortDate);
    html += "</pre>";

    file_info_browser->setHtml(html);
}

void gui_main_window::display_about()
{
    gui_about about_window;

    about_window.exec();
}
