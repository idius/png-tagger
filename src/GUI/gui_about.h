/* PNG Tagger -- a program for editing PNG img header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the gui_about class which creates the About window.
*/

#ifndef GUI_ABOUT_H
#define GUI_ABOUT_H

#include "ui_About.h"
#include "global.h"

class gui_about : public QDialog, public Ui_AboutDialog
{
    Q_OBJECT

public:

    gui_about()
    {
        setupUi(this);
    }
};

#endif // GUI_ABOUT_H
