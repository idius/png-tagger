/* PNG Tagger -- a program for editing PNG img header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


/*! \file
 *  \brief Provides the gui_tag_dialog class which creates the tag dialog box.
 */


#ifndef GUI_TAG_DIALOG_H
#define GUI_TAG_DIALOG_H

#include "ui_tagDialog.h"
#include "global.h"

#include <QString>
#include <QDialog>

class gui_tag_dialog : public QDialog, public Ui_tagDialog
{
    Q_OBJECT

public:

    gui_tag_dialog()
    {
        setupUi(this);
        tag_successful = false;

        connect(buttonBox,
                SIGNAL( accepted() ),
                this,
                SLOT( confirm() ) );

        connect(buttonBox,
                SIGNAL( rejected() ),
                this,
                SLOT( cancel() ) );
    }

    bool successful() { return tag_successful; }

    bool is_person() { return checkBox->isChecked(); }

    QString tag_text() { return lineEdit->text(); }

protected:

    bool tag_successful;

protected slots:

    void cancel() { tag_successful = false; }

    void confirm() { tag_successful = true; }
};

#endif // GUI_TAG_DIALOG_H
