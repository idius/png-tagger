/* PNG Tagger -- a program for editing PNG image header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#include "GUI/preview_scene.h"
#include "GUI/callout.h"
#include "GUI/gui_tag_dialog.h"


void PreviewScene::mousePressEvent(MouseEvent* event)
{
    if ( active_tab == general_tab )
    {
        general_tab_mouse_click_handler(event);
    }

    QGraphicsScene::mousePressEvent(event);
}

void PreviewScene::mouseMoveEvent(MouseEvent* event)
{
    if ( active_tab == general_tab )
    {
        general_tab_mouse_movement_handler(event);
    }

    QGraphicsScene::mouseMoveEvent(event);
}

void PreviewScene::contextMenuEvent(MenuEvent* event)
{
    if ( active_tab == tag_tab && !empty )
    {
        tag_tab_context_menu(event);
    }

    QGraphicsScene::contextMenuEvent(event);
}


void PreviewScene::wheelEvent(WheelEvent* event)
{
    if ( active_tab == tag_tab && !empty )
    {
        tag_tab_wheel(event);
    }

    QGraphicsScene::wheelEvent(event);
}


void PreviewScene::tag_tab_context_menu(QEvent* event)
{
    MenuEvent* menu_event;
    menu_event = static_cast<MenuEvent*>(event);

    current_tag_position = menu_event->scenePos();

    if ( !empty )
    {
        QMenu menu(0);

        removed_callout = identify_callout(current_tag_position);

        if (removed_callout != nullptr)
        {
            menu.addAction(removeAction);
        }
        else
        {
            menu.addAction(tagAction);
        }
        menu.exec(menu_event->screenPos());
    }
}


void PreviewScene::tag_tab_wheel(QEvent* event)
{
    WheelEvent* wheel_event;
    wheel_event = static_cast<WheelEvent*>(event);

    int zoom = wheel_event->delta() / 120;

    scale_callouts(zoom);
}


bool PreviewScene::tag_action_handler()
{

    gui_tag_dialog tag_dialog_box;

    tag_dialog_box.exec();

    if ( !tag_dialog_box.successful() )
    {
        return false;
    }

    if ( tag_dialog_box.tag_text().contains( QRegExp("_|@|\\^|\\|") ) )
    {
        QMessageBox msgBox;
        msgBox.setText("The characters (@_^|) are reserved for internal use.");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        return false;
    }

    if ( !tag_dialog_box.tag_text().contains( QRegExp("\\w") ) )
    {
        QMessageBox msgBox;
        msgBox.setText("Tags must contain at least one alphanumeric character");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        return false;
    }

    QPointF box;

    box = place_new_callout( current_tag_position, tag_dialog_box.tag_text() );

    if (tag_dialog_box.is_person())
    {
        emit sig_append_people_box(tag_dialog_box.tag_text());
    }

    QString link;

    QTextStream entry(&link);

    entry << "^|^" << tag_dialog_box.tag_text() << "@";
    entry << current_tag_position.x() / width() << "_";
    entry << current_tag_position.y() / height() << "_";
    entry << box.x() << "_" << box.y();

    emit sig_append_tag_browser(link);

    return true;
}


bool PreviewScene::remove_action_handler()
{
    const bool result = callout_list.removeOne(removed_callout);

    if (result)
    {
        removeItem(removed_callout);
        delete removed_callout;
        removed_callout = nullptr;
        emit sig_tag_edited(generate_tag_list());
        parent_view->viewport()->update();
    }

    return result;
}


void PreviewScene::tab_changed(int tab_number)
{
    active_tab = tab_id(tab_number);

    if ( active_tab != general_tab )
    {
        emit sig_set_normal_cursor();
        active_cursor = cursor::normal;
    }
}


bool PreviewScene::general_tab_mouse_movement_handler(QEvent* event)
{
    MouseEvent* mouse_event = static_cast<MouseEvent*>(event);

    const float x_coord = mouse_event->scenePos().x();
    const float middle = 0.5 * width();

    if ( (x_coord < middle && active_cursor != cursor::left) && !empty )
    {
        emit sig_set_back_cursor();
        active_cursor = cursor::left;
    }
    else if ( (x_coord > middle && active_cursor != cursor::right) && !empty )
    {
        emit sig_set_forward_cursor();
        active_cursor = cursor::right;
    }

    return false;
}


bool PreviewScene::general_tab_mouse_click_handler(QEvent* event)
{
    MouseEvent* mouse_event = static_cast<MouseEvent*>(event);

    const float x_coord = mouse_event->scenePos().x();
    const float middle = 0.5 * width();

    if ( x_coord < middle )
    {
        emit sig_go_back();
        return false;
    }
    else
    {
        emit sig_go_forward();
        return false;
    }
}


callout* PreviewScene::identify_callout(const QPointF& pos) const
{
    for (int c = 0; c < callout_list.size(); ++c)
    {
        callout* candidate = callout_list.at(c);
        const QPointF local_pos = candidate->mapFromScene(pos);
        if (candidate->contains(local_pos)) { return candidate; }
    }
    return nullptr;
}


void PreviewScene::set_parent(QGraphicsView* _parent)
{
    parent_view = _parent;
}


void PreviewScene::scale_callouts(int zoom)
{
    const float scale_factor = (zoom > 0) ? 1.1 : 0.9;

    QList<callout*>::iterator i;

    for (i = callout_list.begin(); i != callout_list.end(); ++i)
    {
        (*i)->change_scale(scale_factor);
        parent_view->viewport()->update();
    }
}


void PreviewScene::free_callouts()
{
    callout_list.clear();
    callout_strings.clear();
    pin.clear();
}


bool PreviewScene::pin_tag(const QUrl& url)
{
    QString str = url.toString();

    if ( str.contains("*") && str.length() == 1 )
    {
        show_callouts(!callouts_visible);
        return false;
    }

    // valid tags must contain 3 underscores.
    if ( str.count("_") != 3 ) { return false; }

    QStringList coordinates = str.split("_");

    const float x = width() * coordinates[0].toFloat();
    const float y = height() * coordinates[1].toFloat();

    /*! \todo A custom pin class should be designed eventually; it's just a red
     *  square for now.
     */

    QGraphicsRectItem* p = new QGraphicsRectItem(x - 8, y - 8, 16.0, 16.0);

    pin.append(p);

    p->setPen(Qt::NoPen);
    p->setBrush(Qt::red);
    p->setOpacity(0.8);

    QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect;
    effect->setColor(QColor(0, 0, 0, 160));
    effect->setBlurRadius(11.0);
    effect->setXOffset(3.0);
    effect->setYOffset(3.0);
    p->setGraphicsEffect(effect);
    addItem(p);

    // remove the previous pin. This seems like a messy way of peforming this
    // task (i.e., putting the pointer into a list), but this was the only
    // method I have found that behaves properly.

    if ( pin.length() > 1 ) { removeItem(pin[pin.length() - 2]); }

    parent_view->viewport()->update();

    return true;
}

void PreviewScene::show_callouts(bool show)
{
    if ( callout_list.isEmpty()
            && !callout_strings.isEmpty()
            && show
            && !callouts_visible)
    {
        for (int i = 0; i < callout_strings.length(); ++i)
        {
            if ( callout_strings[i].contains("@") )
            {
                QStringList strs = callout_strings[i].split("@");

                // valid tags must contain 3 underscores.
                if ( strs[1].count("_") != 3 ) { break; }

                QString text = strs[0];
                QStringList point = strs[1].split("_");
                QPointF position( width() * point[0].toFloat(),
                                  height() * point[1].toFloat() );
                place_new_callout(position, text);
            }
        }
    }
    else if ( !callout_list.isEmpty() && show && !callouts_visible )
    {
        for (int i = 0; i < callout_list.length(); ++i)
        {
            addItem(callout_list[i]);
        }
    }
    else if ( !callout_list.isEmpty() && !show && callouts_visible )
    {
        for (int i = 0; i < callout_list.length(); ++i)
        {
            removeItem(callout_list[i]);
        }
    }

    parent_view->viewport()->update();
    callouts_visible = show;
}


QPointF PreviewScene::place_new_callout(const QPointF& pos, const QString& text)
{
    callout* p = new callout( pos, text, this );

    callout_list.append(p);

    addItem(p);

    return p->box().topLeft();

    callouts_visible = true;
}


void PreviewScene::add_callout_string(const QString& str)
{
    callout_strings.append(str);
}


void PreviewScene::clear_all()
{
    clear();
    empty = true;
    emit sig_set_normal_cursor();
    active_cursor = cursor::normal;
}


QGraphicsPixmapItem* PreviewScene::add_pixmap(const QPixmap& pixmap)
{
    empty = false;
    return addPixmap(pixmap);
}


void PreviewScene::tag_edited(const callout* tag)
{
    /*! \todo make it possible to remove this callout using this pointer. */
    Q_UNUSED(tag);

    emit sig_tag_edited(generate_tag_list());
}


QString PreviewScene::generate_tag_list() const
{
    QString link;

    QTextStream entry(&link);

    for (int i = 0; i < callout_list.size(); ++i)
    {
        entry << "^|^" << callout_list[i]->text() << "@";
        entry << callout_list[i]->target().x() / width() << "_";
        entry << callout_list[i]->target().y() / height() << "_";
        entry << callout_list[i]->box().x() << "_" << callout_list[i]->box().y();
    }

    return link;
}
