/* PNG Tagger -- a program for editing PNG img header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "GUI/callout.h"
#include "preview_scene.h"

#include <QtGui>

callout::callout(const QPointF& point,
                 const QString& name,
                 PreviewScene* parent)
{
    setPos(point);
    target_point = point;
    parent_scene = parent;

    // approximate the dimensions of the box

    const float r_height = 27 * ( 1 + name.size() / 16.0 );

    QRectF rect(-35, -25 - r_height, 180, r_height);

    label_box = rect;
    label_text = name;
    zoom_scale = 1.0;
    effect = new QGraphicsDropShadowEffect;
    effect->setColor(QColor(0, 0, 0, 200));
    effect->setBlurRadius(11.0);
    effect->setXOffset(4.0);
    effect->setYOffset(4.0);
    setGraphicsEffect(effect);
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
}


QRectF callout::boundingRect() const
{
    const QPointF local_target = mapFromScene(target_point);
    const QPointF box_top_left = label_box.topLeft();
    const QPointF box_bottom_right = label_box.bottomRight();

    const float top_left_x = box_top_left.x();
    const float top_left_y = box_top_left.y();
    const float bottom_right_x = box_bottom_right.x();
    const float bottom_right_y = box_bottom_right.y();
    const float target_x = local_target.x();
    const float target_y = local_target.y();

    float left, right, top, bottom;

    if (top_left_x < target_x)
    {
        left = top_left_x;
        right = (target_x > bottom_right_x) ? target_x : bottom_right_x;
    }
    else
    {
        left = target_x;
        right = bottom_right_x;
    }

    if (top_left_y < target_y)
    {
        top = top_left_y;
        bottom = (target_y > bottom_right_y) ? target_y : bottom_right_y;
    }
    else
    {
        top = target_y;
        bottom = bottom_right_y;
    }

    return QRectF(QPointF(left - 10, top - 10),
                  QPointF(right + 10, bottom + 10)
                 );
}


void callout::paint(QPainter* painter,
                    const QStyleOptionGraphicsItem* option,
                    QWidget* widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    const QPointF local_target = mapFromScene(target_point);

    QPen pen;

    pen.setColor(Qt::black);
    pen.setJoinStyle(Qt::RoundJoin);
    pen.setWidth(1);

    painter->setRenderHint(QPainter::Antialiasing);
    painter->setBrush(QBrush(QColor(72, 99, 152)));
    painter->setPen(pen);
    painter->setOpacity(0.7);

    //Paint the callout box

    QPainterPath callout_path;

    callout_path.setFillRule(Qt::WindingFill);

    callout_path.addRoundedRect(label_box, 10.0, 10.0);

    callout_path.moveTo(local_target);

    if (local_target.x() < label_box.x() - 10)
    {
        callout_path.lineTo(label_box.x() + 0.33 * label_box.width(),
                            label_box.y() + 0.01 * label_box.height());

        callout_path.lineTo(label_box.x() + 0.33 * label_box.width(),
                            label_box.y() + 0.99 * label_box.height());
    }
    else if (local_target.x() > label_box.right() + 10)
    {
        callout_path.lineTo(label_box.right() - 0.33 * label_box.width(),
                            label_box.bottom() - 0.01 * label_box.height());

        callout_path.lineTo(label_box.right() - 0.33 * label_box.width(),
                            label_box.bottom() - 0.99 * label_box.height());
    }
    else if (local_target.y() < label_box.center().y())
    {
        callout_path.lineTo(label_box.right() - 0.33 * label_box.width(),
                            label_box.center().y());

        callout_path.lineTo(label_box.x() + 0.33 * label_box.width(),
                            label_box.center().y());
    }
    else if (local_target.y() > label_box.center().y())
    {
        callout_path.lineTo(label_box.x() + 0.33 * label_box.width(),
                            label_box.center().y());

        callout_path.lineTo(label_box.right() - 0.33 * label_box.width(),
                            label_box.center().y());
    }

    callout_path.closeSubpath();

    painter->drawPath(callout_path.simplified());

    //Paint the text inside of the callout box

    QRectF text_box(label_box.x() + 9,
                    label_box.y(),
                    label_box.width() - 18,
                    label_box.height()
                   );

    painter->setOpacity(1.0);
    painter->setPen(Qt::white);
    painter->drawText(text_box,
                      Qt::AlignCenter | Qt::TextWordWrap,
                      label_text
                     );
}


void callout::change_scale(float scale_factor)
{
    const float s = zoom_scale * scale_factor;

    if ( s < 4 && s > 0.5 )
    {
        zoom_scale = s;
    }

    setScale(zoom_scale);
}


void callout::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}


void callout::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}


QVariant callout::itemChange(GraphicsItemChange change, const QVariant& value)
{
    if (change == ItemPositionChange)
    {
        parent_scene->tag_edited(this);
        update();
    }

    return QGraphicsItem::itemChange(change, value);
}
