/* PNG Tagger -- a program for editing PNG img header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the gui_main_window class, which creates PNG Tagger's  Main
           Application window GUI.
*/


#ifndef INC_GUI_MAIN_H
#define INC_GUI_MAIN_H


#include "ui_MainWindow.h"
#include "global.h"
#include "GUI/preview_scene.h"
#include "GUI/gui_about.h"

#include <QtGui>

#include <iostream>

/*! \brief Creates the Main window.*/
class gui_main_window : public QMainWindow, public Ui_MainWindow
{
    Q_OBJECT

protected:

    QApplication*   application;
    QAction*        show_nextAction;
    QAction*        show_previousAction;
    QAction*        zoom_out_sceneAction;
    QAction*        zoom_in_sceneAction;
    QString         date_field;
    QString         people_field;
    QString         location_field;
    QString         description_field;
    QString         tag_field;
    QString         current_directory;
    QString         current_file_name;
    QString         output_file_name;
    QStringList     file_list;
    QImage          current_img;
    QPixmap         current_pixmap;
    PreviewScene    scene;
    float           current_scale;
    int             first_img_index;
    int             current_img_index;
    int             current_img_width;
    int             current_img_height;
    bool            modified;

public:

    gui_main_window(QApplication* app, QMainWindow* parent = 0);

    virtual ~gui_main_window();

    void resizeEvent ( QResizeEvent* event );

protected:

    /*! Connects all necessary signals and slots and creates the actions needed
        by the main application window.*/
    void set_connections()
    {
        setupUi(this);

        connect( actionOpen,
                 SIGNAL(triggered()),
                 this,
                 SLOT( open_first_img() )
               );

        connect( actionSave,
                 SIGNAL(triggered()),
                 this,
                 SLOT( save_img() )
               );

        connect( actionClose,
                 SIGNAL(triggered()),
                 this,
                 SLOT( close_img() )
               );

        connect( actionAbout_PNG_Tagger,
                 SIGNAL(triggered()),
                 this,
                 SLOT( display_about() )
               );

        connect( date_line,
                 SIGNAL(textChanged(QString)),
                 this,
                 SLOT( set_date_field(QString) )
               );

        connect( people_box,
                 SIGNAL(textChanged() ),
                 this,
                 SLOT( set_people_field() )
               );

        connect( location_line,
                 SIGNAL(textChanged(QString) ),
                 this,
                 SLOT( set_location_field(QString) )
               );

        connect( description_box,
                 SIGNAL(textChanged() ),
                 this,
                 SLOT( set_description_field() )
               );

        connect( &scene,
                 SIGNAL( sig_go_forward() ),
                 this,
                 SLOT( open_next_img() )
               );

        connect( &scene,
                 SIGNAL( sig_go_back() ),
                 this,
                 SLOT( open_previous_img() )
               );

        connect( &scene,
                 SIGNAL( sig_set_back_cursor() ),
                 this,
                 SLOT( set_back_cursor() )
               );

        connect( &scene,
                 SIGNAL( sig_set_forward_cursor() ),
                 this,
                 SLOT( set_forward_cursor() )
               );

        connect( &scene,
                 SIGNAL( sig_set_normal_cursor() ),
                 this,
                 SLOT( set_normal_cursor() )
               );

        connect( &scene,
                 SIGNAL( sig_append_people_box(const QString&) ),
                 this,
                 SLOT ( append_people_box(const QString&) )
               );

        connect( &scene,
                 SIGNAL( sig_tag_edited(QString) ),
                 this,
                 SLOT( set_tag_browser(QString) )
               );

        connect( &scene,
                 SIGNAL( sig_append_tag_browser(QString) ),
                 this,
                 SLOT( append_tag_browser(QString) )
               );

        connect( saveButton,
                 SIGNAL( clicked() ),
                 this,
                 SLOT( save_img() )
               );

        connect( saveButton2,
                 SIGNAL( clicked() ),
                 this,
                 SLOT( save_img() )
               );

        connect( tabWidget,
                 SIGNAL ( currentChanged(int) ),
                 &scene,
                 SLOT( tab_changed(int) )
               );

        connect( tag_browser,
                 SIGNAL (anchorClicked (const QUrl&) ),
                 &scene,
                 SLOT ( pin_tag(const QUrl&) ) );

        show_nextAction = new QAction(tr("Show &Next"), this);
        show_nextAction->setShortcut(QKeySequence::MoveToNextChar);
        show_nextAction->setStatusTip(tr("Load the next image"));
        addAction(show_nextAction);

        connect( show_nextAction,
                 SIGNAL( triggered() ),
                 this,
                 SLOT( open_next_img() )
               );

        show_previousAction = new QAction(tr("Show &Previous"), this);
        show_previousAction->setShortcut(QKeySequence::MoveToPreviousChar);
        show_previousAction->setStatusTip(tr("Load the previous image"));
        addAction(show_previousAction);

        connect( show_previousAction,
                 SIGNAL( triggered() ),
                 this,
                 SLOT( open_previous_img() )
               );

        zoom_in_sceneAction = new QAction(tr("Zoom Image &In"), this);
        zoom_in_sceneAction->setShortcut(Qt::Key_Plus);
        addAction(zoom_in_sceneAction);

        connect( zoom_in_sceneAction,
                 SIGNAL( triggered() ),
                 this,
                 SLOT( zoom_in() )
               );

        // note: use QKeySequence() for combinations like Ctrl+Key_Plus

        zoom_out_sceneAction = new QAction(tr("Zoom Image &Out"), this);
        zoom_out_sceneAction->setShortcut(Qt::Key_Minus);
        addAction(zoom_out_sceneAction);

        connect( zoom_out_sceneAction,
                 SIGNAL( triggered() ),
                 this,
                 SLOT( zoom_out() )
               );
    }

    /*! Sets the modified attribute to mod. If the user tries to load a new
        image without saving while modified==true, an error message displays. */
    void set_modified(bool mod);

    /*! Displays an eror message notifying the user that they did not save their
        changes. */
    void warn_unsaved();

    /*! Displays the image info in the info tab.*/
    void file_info();

public slots:

    /*! Opens a file chooser dialog to choose a file name, then opens the file.
        If the parent directory contains more than one image, stores a list of
        all file names so that the next and previous buttons can load the other
        images in the directory without using the file chooser dialog. */
    int  open_first_img();

    /*! Opens the next image in the file_list (i.e., in the active directory).*/
    void open_next_img();

    /*! Opens the previous image in the current directory*/
    void open_previous_img();

    /*! Opens the image named in the string, img_name, reads its metadata, and
        displays data in the preview panel. The image preview is scaled so that
        the entire image is visible in the preview panel.*/
    void open_img(const QString&);

    /*! Saves the modified image to disk. */
    bool save_img();

    /*! Opens a file chooser dialog, chooses a file name, saves image to disk.*/
    int save_as();

    /*! Closes the current image and diectory.*/
    void close_img();

    /*! Sets the internal date_field string.*/
    void set_date_field(const QString&);

    /*! Sets the internal people_field string.*/
    void set_people_field();

    /*! Appends the name stored in the string to the people box.*/
    void append_people_box(const QString&);

    /*! Sets tag_field string to tag and parse the string so that it can be
        displayed as hyperlinks in the tag_browser of the main window. Returns
        true if successful. */
    bool parse_tag_field(QString tag);

    /*! Sets the internal location_field string.*/
    void set_location_field(const QString&);

    /*! Sets the internal description_field string.*/
    void set_description_field();

    /*! Appends a hyperlink to the tag browser. */
    void append_tag_browser(QString);

    /*! sets the content of the tag browser. */
    void set_tag_browser(QString);

    /*! Changes the cursor to a rightward / forward arrow.*/
    void set_forward_cursor();

    /*! Changes the cursor to a leftward / back arrow.*/
    void set_back_cursor();

    /*! Changes the cursor to a normal arrow.*/
    void set_normal_cursor();

    /*! Zooms the PreviewScene (scene) in.*/
    void zoom_in();

    /*! Zooms the PreviewScene (scene) out.*/
    void zoom_out();

    /*! Displays the About window.*/
    void display_about();
};


#endif // GUI_MAIN_H
