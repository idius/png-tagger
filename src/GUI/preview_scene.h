/* PNG Tagger -- a program for editing PNG image header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


/*! \file
 *  \brief Provides the PreviewScene class.
 */

#ifndef PREVIEW_SCENE_H
#define PREVIEW_SCENE_H

#include "global.h"

#include <QtGui>

class callout;

/*! A custom QGraphicsScene subclass for handling custom display behavior in the
    image preview pane (i.e., the QGraphicsView object in the main GUI window).
 */
class PreviewScene : public QGraphicsScene
{
    Q_OBJECT

public:

    typedef QGraphicsSceneMouseEvent         MouseEvent;
    typedef QGraphicsSceneContextMenuEvent   MenuEvent;
    typedef QGraphicsSceneWheelEvent         WheelEvent;

private:

    QAction*            tagAction;
    QAction*            removeAction;
    QList<callout*>     callout_list;
    callout*            removed_callout = nullptr;
    QList<QGraphicsRectItem*> pin;
    QGraphicsView*      parent_view;
    QPointF             current_tag_position;
    QStringList         callout_strings;
    enum                tab_id { general_tab,
                                 tag_tab,
                                 file_info_tab
                               } active_tab;
    enum class          cursor { normal,
                                 left,
                                 right
                               } active_cursor;
    bool                callouts_visible;
    bool                empty;

    /*! Handles events such as mouse clicks and mouse motions encountered by
        this object.
     */
    void mousePressEvent(MouseEvent* event);

    void mouseMoveEvent(MouseEvent* event);

    void contextMenuEvent(MenuEvent* event);

    void wheelEvent(WheelEvent* event);


    /*! Handles context menu (typically, left mouse clicks) when the tag tab is
        active. This allows the user to tag the photo at a specific location.
     */
    void tag_tab_context_menu(QEvent* event);

    /*! Handles mouse wheel scroll events. */
    void tag_tab_wheel(QEvent* event);

    /*! Tracks the location of the mouse when the general tab is active and
        changes the cursor to the appropriate shape. A left arrow displays when
        the cursor hovers over the left half of the image, while a the cursor
        becomes a right arrow when hovering over the right half of the image.
     */
    bool general_tab_mouse_movement_handler(QEvent* event);

    /*! Handles mouse clicks when the general tab is active. */
    bool general_tab_mouse_click_handler(QEvent* event);

    /*!
        Returns a pointer to the callout located at pos. If no callout exists
        under pos, then returns nullptr.
     */
    callout* identify_callout(const QPointF& pos) const;


public:

    PreviewScene()
    {
        active_cursor = cursor::normal;
        callouts_visible = false;
        empty = true;
        active_tab = general_tab;

        tagAction = new QAction(tr("&Tag person or item"), this);

        connect( tagAction,
                 SIGNAL( triggered() ),
                 this,
                 SLOT( tag_action_handler() )
               );

        removeAction = new QAction(tr("&Remove this tag"), this);

        connect( removeAction,
                 SIGNAL( triggered() ),
                 this,
                 SLOT( remove_action_handler() )
               );
    }

    ~PreviewScene()
    {
        delete tagAction;
        delete removeAction;
    }

    /*! Informs this scene of its parent GraphicsView pointer so that the scene
        can manipulate it's GraphicsView. Note: If a signal were implemented
        and connected to the GraphicsView viewport()->update() slot in the
        gui_main.h, this member would no longer be needed.
     */
    void set_parent(QGraphicsView* _parent);

    /*! Appends its argument to the callout_strings list. */
    void add_callout_string(const QString&);

    /*! Adds a callout with caption, text, to the scene at position, pos.
        Returns the position of the upper-left corner of the new callout box.
     */
    QPointF place_new_callout(const QPointF& pos, const QString& text);

public slots:

    /*! Performs tasks required to begin tagging the image.*/
    bool tag_action_handler();

    /*! Removes the callout from the scene. */
    bool remove_action_handler();

    /*! Handles tab changes so that the left and right click behavior is correct
        on this PreviewScene.
     */
    void tab_changed(int);

    /*! Scales callouts by the factor zoom, such that zoom=2 causes the linear
        dimensions of the callout to double.
     */
    void scale_callouts(int zoom);

    /*! Deletes all callouts from the scene. */
    void free_callouts();

    /*! Interprets the formatted string, url, and places an indicator centered
        at the location specified in the url.
     */
    bool pin_tag(const QUrl& url);

    /*! Displays callouts if show == true and hides them if show == false. */
    void show_callouts(bool show);

    /*! Clears the scene and sets empty=true. */
    void clear_all();

    /*! Adds a pixmap item and sets empty=false. */
    QGraphicsPixmapItem* add_pixmap(const QPixmap& pixmap);

    /*! Emits the sig_tag_edited signal. */
    void tag_edited(const callout* tag);

    /*! Returns a string containing tag links based on the current contents of
        the callout_list. In other words, this creates a new list of tag links
        based on the actual contents of the scene.
     */
    QString generate_tag_list() const;

signals:

    /*! Tells the main window to load the previous image. */
    void sig_go_back();

    /*! Tells the main window to load the next image. */
    void sig_go_forward();

    /*! Changes the cursor to a leftward pointing arrow. */
    void sig_set_back_cursor();

    /*! Changes the cursor to a rightward pointing arrow. */
    void sig_set_forward_cursor();

    /*! Changes the cursor to the default / standard pointer. */
    void sig_set_normal_cursor();

    /*! Appends its argument to the People line of the general tab. */
    void sig_append_people_box(const QString&);

    /*! Informs the main window that a tag has been edited, so that the change
        can be saved. The argument is a string containing the entire list of
        tag links.
     */
    void sig_tag_edited(const QString);

    /*! Appends a link to the tab browser on the tag tab. */
    void sig_append_tag_browser(const QString&);
};

#endif // PREVIEW_SCENE_H
