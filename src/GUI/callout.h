/* PNG Tagger -- a program for editing PNG img header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/*! \file
    \brief Provides the callout class which creates callout lables in the
           PreviewScene.
*/

#ifndef CALLOUT_H
#define CALLOUT_H

#include <QGraphicsItem>
#include <QPoint>
#include <QGraphicsDropShadowEffect>
#include <QVariant>

class PreviewScene;

/*! Callout objects can be placed in a preview_scene object to label a person or
    object in the scene.

    \todo Finish writing this class so that callouts are movable with the mouse.

*/
class callout : public QGraphicsItem
{
public:

    callout(const QPointF& point,
            const QString& name,
            PreviewScene* parent
           );

    /*! Scales the callout linearly by scale_factor. */
    void change_scale(float scale_factor);

    QPointF target() const { return target_point; }

    QRectF  box() const { return label_box; }

    QString text() const { return label_text; }

protected:

    QPointF                    target_point;
    QString                    label_text;
    QRectF                     label_box;
    float                      zoom_scale;
    PreviewScene*              parent_scene;
    QGraphicsDropShadowEffect* effect;

    QRectF boundingRect() const;

    void paint(QPainter* painter,
               const QStyleOptionGraphicsItem* option,
               QWidget* widget
              );

    void mousePressEvent(QGraphicsSceneMouseEvent* event);

    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);

    QVariant itemChange(GraphicsItemChange change, const QVariant& value);
};

#endif // CALLOUT_H

// example: http://doc.qt.digia.com/stable/graphicsview-elasticnodes.html
