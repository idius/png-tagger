/* PNG Tagger -- a program for editing PNG image header tags.
Copyright (C) 2013 Nathaniel R Stickley (idius@idius.net).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


/*! \file
 *  \brief Global function and data type / structure declarations. All global
 *         functions are in the Pt namespace.
 */

#ifndef GLOBAL_H
#define GLOBAL_H

#include <string>

class QString;

typedef unsigned int uint;

namespace Pt
{

/*! Draws attention to text in a terminal / console by dislaying the text in
    bold red font.*/
void attention(const char* text, const std::string &msg);

/*! Checks whether file is a png image. */
bool is_PNG(const char* file);

/*! Tests whether the argument string represents a numeric type. Returns
    true only if the string pointed to by str represents a numerical value.*/
bool is_numeric(const char* str);

bool is_valid_tag(const QString& tag);

/*! Prints the warning message string.*/
void print_warning(const std::string& msg);
void print_warning(const char* _msg);

/*! Prints the error message string and exits with the specified status*/
void print_error(const std::string& msg, int status);
void print_error(const char* _msg, int status);

}// namespace Pt

#endif // GLOBAL_H
