# http://pepper.troll.no/s60prereleases/doc/qmake-variable-reference.html
# http://pepper.troll.no/s60prereleases/doc/qmake-manual.html

# Note: this currently assumes that the code will be compiled using GCC (g++).

TEMPLATE = app
TARGET = png-tagger
#QT += webkit

# specify a release or debug build
CONFIG += release

DEPENDPATH += .
INCLUDEPATH += .
QMAKE_LIBS +=

# specific settings for release builds. Comment out if building with an older
# compiler that does not support the -Ofast optimization level (like the MinGW
# compiler used for Windows).
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast              \
                          -std=c++11          \
                          -ffunction-sections \
                          -fdata-sections     \
                          -fmodulo-sched      \
                          -fno-exceptions     \
                          -fno-rtti

QMAKE_CFLAGS_RELEASE   -= -O2
QMAKE_CFLAGS_RELEASE   += -Ofast
QMAKE_LFLAGS_RELEASE    = -Ofast

# Input
HEADERS   += CLI/cli_actions.h      \
             global.h               \
             GUI/preview_scene.h    \
             GUI/callout.h          \
             GUI/gui_main.h         \
             GUI/gui_tag_dialog.h   \
             GUI/gui_about.h

FORMS     += UIs/MainWindow.ui      \
             UIs/tagDialog.ui       \
             UIs/About.ui

SOURCES   += main.cc                \
             CLI/cli_actions.cc     \
             global.cc              \
             GUI/gui_main.cc        \
             GUI/preview_scene.cc   \
             GUI/callout.cc

RESOURCES += resources/images.qrc

# The following is specific to machines with UNIX-like file systems. Comment out
# if compiling on Windows.

icons.path = /usr/share/icons/png-tagger/
icons.files = resources/*.png resources/*.svg
launcher.path = /usr/share/applications/
launcher.files = resources/png-tagger.desktop
target.path = /usr/local/bin/

INSTALLS += icons launcher target
